//
//  RegistrationViewController.swift
//  iosvirtualassistantapp
//
//  Created by admin on 14/04/21.
//  Copyright © 2021 IBM. All rights reserved.
//

import Foundation
import UIKit

class RegistrationViewController : UIViewController {
    
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldBenefID: UITextField!
    @IBOutlet weak var txtFieldLocation: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        ViewController.userName = txtFieldName.text ?? "Rushi"
    }
    
}

extension RegistrationViewController : UITextFieldDelegate {
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        txtFieldName.resignFirstResponder()
        txtFieldLocation.resignFirstResponder()
        txtFieldBenefID.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
